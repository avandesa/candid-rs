/// Represents a basic CAN Frame containing the frame's id and data
#[derive(Debug, Copy, Clone)]
pub struct Frame {
    pub id: u32,
    pub data: [u8; 8],
}

impl Frame {
    /// Creates a new frame with the given id and data
    pub fn new(id: u32, data: [u8; 8]) -> Frame {
        Frame { id, data }
    }

    pub fn to_string(&self) -> String {
        format!("{:?}", &self)
    }
}

use candid_client::*;

use rand::prelude::*;

use clap::{App, Arg};

fn main() {
    let matches = App::new("CANdid Client")
        .version("0.1.0")
        .author("Alex van de Sandt <avandesa@purdue.edu>")
        .about("A client to send messages to a CANdid server")
        .arg(
            Arg::with_name("addr")
                .value_name("ADDRESS")
                .help("The address:port to connect to")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let addr = matches.value_of("addr").unwrap();

    let mut client = CandidConnection::new(addr).expect("Couldn't connect to server");

    for i in 1..10 {
        let data = random::<u64>().to_ne_bytes();
        let frame = Frame::new(i, data);
        println!("Sending {:?}", frame);
        client.write_frame(Frame::new(i, data)).unwrap();
    }

    loop {
        let frame = client.read_frame().unwrap();
        println!("{:?}", frame);
    }
}

use candid_client::*;

use clap::{App, Arg};

fn main() {
    let matches = App::new("CANdid Client")
        .version("0.1.0")
        .author("Alex van de Sandt <avandesa@purdue.edu>")
        .about("A client to read messages from a CANdid server")
        .arg(
            Arg::with_name("addr")
                .value_name("ADDRESS")
                .help("The address:port to connect to")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let addr = matches.value_of("addr").unwrap();

    let mut client = CandidConnection::new(addr).expect("Couldn't connect to server");

    loop {
        let frame = client.read_frame().unwrap();
        println!("{:?}", frame);
    }
}

extern crate rand;
extern crate socketcan;

use rand::prelude::*;
use socketcan::{CANFrame, CANSocket};
use std::thread;
use std::time::Duration;

fn main() {
    // Open a write socket
    let write_socket = CANSocket::open("vcan0").unwrap();
    write_socket.set_write_timeout(Duration::new(5, 0)).unwrap();

    for i in 1..10 {
        let data: u64 = random();
        let data = data.to_ne_bytes();

        write_socket
            .write_frame(&CANFrame::new(i + 0x123, &data, false, false).unwrap())
            .unwrap();

        thread::sleep(Duration::from_millis(100));
    }
}

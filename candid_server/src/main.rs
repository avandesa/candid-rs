//! # CANdid Server
//!
//! Reads from the specified CAN bus and relays each frame to connected clients.
//!
//! # Usage
//!
//! ```
//! candid_server <ADDRESS> <INTERFACE>
//!
//! FLAGS:
//!     -h, --help       Prints help information
//!     -V, --version    Prints version information
//!
//! ARGS:
//!     <ADDRESS>      The address:port to bind to
//!     <INTERFACE>    The CAN interface to listen to
//! ```

extern crate candid_server;

use candid_server::{can_reader::CANReader, client_handler::new_client_handler};

use std::net::TcpListener;
use std::sync::mpsc;

use socketcan::CANSocket;

use clap::{App, Arg};

fn main() {
    let matches = App::new("CANdid Server")
        .version("0.1.0")
        .author("Alex van de Sandt <avandesa@purdue.edu>")
        .about("A server to read, parse, and relay messages from a CAN bus.")
        .arg(
            Arg::with_name("addr")
                .value_name("ADDRESS")
                .help("The address:port to bind to")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("bus")
                .value_name("INTERFACE")
                .help("The CAN interface to listen to")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    // Process commandline args
    let addr = matches.value_of("addr").unwrap();
    let can_interface = matches.value_of("bus").unwrap();

    // Open CAN Socket
    let can_socket = CANSocket::open(can_interface).expect("Could not open CAN socket");

    // Instantiate CANReader
    let (can_reader, _) = CANReader::new(can_socket);

    // Open TCPListener
    let listener = TcpListener::bind(addr).expect("Could not bind to address.");

    let mut clients = Vec::new();

    loop {
        // Listen for new connections
        if let Ok((socket, addr)) = listener.accept() {
            println!("New Client: {:?}", addr);

            // Create channel. Sender -> CANReader, Receiver -> ClientHandler.
            let (tx, rx) = mpsc::channel();
            can_reader.add_client(tx);

            // Create client handler
            clients.push(new_client_handler(socket, (*can_interface).to_string(), rx));
        }
    }
}

# CANdid

A client and server for reading, parsing, and relaying messages from a Controller Area Network bus across a network.

Author: [Alex van de Sandt](https://avandesa.github.io) (avandesa@purdue.edu)

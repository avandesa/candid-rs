use crate::app::AppState;

use std::io;

use tui::backend::Backend;
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, SelectableList, Widget};
use tui::Terminal;

pub fn draw<B: Backend>(terminal: &mut Terminal<B>, app: &AppState) -> Result<(), io::Error> {
    terminal.draw(|mut f| {
        let size = f.size();
        // List all the frames received to this point
        SelectableList::default()
            .block(Block::default().borders(Borders::ALL).title(app.title))
            .items(&app.frame_history.items)
            .select(Some(app.frame_history.selected))
            .highlight_style(Style::default().fg(Color::Yellow).modifier(Modifier::BOLD))
            .highlight_symbol(">")
            .render(&mut f, size)
    })
}

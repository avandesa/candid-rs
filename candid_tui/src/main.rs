use candid_tui::*;

use candid_client::*;

use std::io;

use clap::{App, Arg};

use termion::event::Key;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;

use tui::backend::TermionBackend;
use tui::Terminal;

fn main() {
    let matches = App::new("CANdid Client")
        .version("0.1.0")
        .author("Alex van de Sandt <avandesa@purdue.edu>")
        .about("A client to read messages from a CANdid server")
        .arg(
            Arg::with_name("addr")
                .value_name("ADDRESS")
                .help("The address:port to connect to")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let addr = matches.value_of("addr").unwrap();

    let server = CandidConnection::new(addr).expect("Couldn't connect to server");

    // Initialize tui
    let stdout = io::stdout().into_raw_mode().unwrap();
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend).unwrap();
    terminal.hide_cursor().unwrap();

    // Initialize the app state
    let mut app = AppState::new("CANdid TUI");

    // Initialize the event aggregator
    let events = Events::new(server);

    loop {
        // Refresh the ui
        candid_tui::draw(&mut terminal, &app).unwrap();

        // Handle an incoming event
        match events.next().unwrap() {
            Event::Input(key) => match key {
                Key::Char(c) => app.on_key(c),
                Key::Up => app.on_up(),
                Key::Down => app.on_down(),
                _ => {}
            },
            Event::Frame(frame) => app.frame_history.push(frame),
        }

        // Exit if necessary, set in app.on_key(q)
        if app.should_quit {
            break;
        }
    }
}

//! A module for configuring and executing event loops. An `Events` instance will listen to `stdin
//! for user input and read frames from the server, aggregating these events to be sent back to the
//! app itself.
//!
//! All code in this module is *heavily* inspired by [`tui-rs`'s demo
//! code](https://github.com/fdehau/tui-rs/blob/master/examples/util/event.rs)

use crate::util::FrameInfo;

use std::sync::mpsc;
use std::{io, thread};

use candid_client::*;

use termion::event::Key;
use termion::input::TermRead;

pub enum Event<I> {
    Input(I),
    Frame(FrameInfo),
}

/// An event handler that wraps termion input and tick events.
/// Each event thype is handled in its own thread and returned to a common `Receiver`
pub struct Events {
    /// The receiver that aggregates all other events
    rx: mpsc::Receiver<Event<Key>>,

    /// Thread that reads input from the user
    _input_handle: thread::JoinHandle<()>,

    /// Thread that reads frames from the remote CANdid server
    _frame_handle: thread::JoinHandle<()>,
}

impl Events {
    /// Creates a new event handle
    pub fn new(mut server: CandidConnection) -> Events {
        let (tx, rx) = mpsc::channel();

        let _input_handle = {
            let tx = tx.clone();
            thread::spawn(move || {
                let stdin = io::stdin();
                for evt in stdin.keys() {
                    match evt {
                        Ok(key) => {
                            if let Err(_) = tx.send(Event::Input(key)) {
                                return;
                            }
                            if key == Key::Char('q') {
                                return;
                            }
                        }
                        Err(_) => {}
                    }
                }
            })
        };

        let _frame_handle = {
            let tx = tx.clone();
            thread::spawn(move || loop {
                let frame = server.read_frame().unwrap();
                let frame = FrameInfo(frame, format!("{:?}", frame).to_string());

                tx.send(Event::Frame(frame)).unwrap();
            })
        };

        Events {
            rx,
            _input_handle,
            _frame_handle,
        }
    }

    pub fn next(&self) -> Result<Event<Key>, mpsc::RecvError> {
        self.rx.recv()
    }
}

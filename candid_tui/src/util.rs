use candid_client::Frame;

/// Keeps track of the position in a list, ued for SelectableLists
pub struct ListState<I> {
    pub items: Vec<I>,
    pub selected: usize,
}

impl<I> ListState<I> {
    /// Initialize a new ListState with no items
    pub fn new() -> ListState<I> {
        ListState {
            items: Vec::new(),
            selected: 0,
        }
    }

    /// Add an item to the end of the list
    pub fn push(&mut self, item: I) {
        self.items.push(item);
    }

    /// Select the previous item in the list
    pub fn previous(&mut self) {
        if self.selected > 0 {
            self.selected -= 1;
        }
    }

    /// Select the next item in the list
    pub fn next(&mut self) {
        if self.selected < self.items.len() - 1 {
            self.selected += 1
        }
    }
}

/// Holds the Frame and its string representation
pub struct FrameInfo(pub Frame, pub String);

impl AsRef<str> for FrameInfo {
    /// Allows the FrameInfo to be used in a SelectableList
    fn as_ref(&self) -> &str {
        &self.1
    }
}

//! Module for maintaining app state

use crate::util::{FrameInfo, ListState};

/// Container for everything related to application state
pub struct AppState<'a> {
    /// The title to be displayed in the top left
    pub title: &'a str,

    /// Whether or not the app should terminate
    pub should_quit: bool,

    /// The frames that have been received from the server to this point
    pub frame_history: ListState<FrameInfo>,
}

impl<'a> AppState<'a> {
    /// Initialize a new app
    pub fn new(title: &'a str) -> AppState<'a> {
        AppState {
            title,
            should_quit: false,
            frame_history: ListState::new(),
        }
    }

    pub fn on_key(&mut self, c: char) {
        match c {
            'q' => self.should_quit = true,
            _ => {}
        }
    }

    pub fn on_up(&mut self) {
        self.frame_history.previous();
    }
    pub fn on_down(&mut self) {
        self.frame_history.next();
    }
}
